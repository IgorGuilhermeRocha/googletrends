/** objeto usado para fazer as reqisições */
const googleTrends = require('google-trends-api');


/** função que pesquisa o interresse sobre um determinado tópico */
async function interestByRegion(keywordAux) {
    let validos = [];
    await googleTrends.interestByRegion({ keyword: keywordAux, startTime: new Date('2022-01-01'), endTime: new Date(), resolution: 'COUNTRY' })
        .then((res) => {
            jsonRes = JSON.parse(res);
            validos = pegaValidos(jsonRes.default.geoMapData);
        })
        .catch((err) => {
            return 'ERROR';
        })

    return validos;
}

/** pega todos os paises com interresse maior que 0 */
function pegaValidos(paises) {
    let validos = [];

    for (let i = 0; i < paises.length; i++) {
        if (paises[i].value[0] > 0) {
            validos.push(paises[i]);
        }
    }

    return validos;

}

// exporta a função
module.exports.interestByRegion = interestByRegion;


