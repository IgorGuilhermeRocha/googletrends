const express = require('express');
const app = express();
const path = require('path');
const trends = require('./trends.js');

/** seta o diretorio dos arquivos estaticos */
app.use(express.static(path.join(__dirname + '/assets'), {
    index: false,
    immutable: true,
    cacheControl: true,
    maxAge: "30d"
}));

/** seta o diretorio das views */
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname + '/views'));

/** endpoint padrao */
app.get('/', async (req, res) => {
    const maiores = []
    res.render('home.ejs', { maiores });

});

/** endpoint da pesquisa do usuário */
app.get('/:termo', async (req, res) => {
    const { termo } = req.query;
    const maiores = await trends.interestByRegion(termo);
    res.render('home.ejs', { maiores });
});

/** servidor escutando na porta 3000 */
app.listen(3000, () => { });


